import {Character} from './Character';

export class Account
{
    public email: string;
    private password: string;
    public character: Character;
    private static qtdCharacters: number = 0;


    constructor(email: string, password: string, character: Character)
    {
        this.email = email;
        this.password = password;
        this.character = character;
        Account.qtdCharacters += 1;
    }

    public static getQtdCharacters(): number{
        return Account.qtdCharacters;
    }

    
}
let ohimeGuerreira :Character = new Character("Female", "Ohime Guerreira", "Tauren", "Engeener", 210000000);
let gabrieldwho = new Account("gabrieldag96@gmail.com", "lalalala", ohimeGuerreira);
ohimeGuerreira.saldo_gold();
ohimeGuerreira.entrada_ou_saida_de_gold(500);