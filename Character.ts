
export class Character 
{
    public gender: string;
    public nickname: string;
    public race: string;
    public profession: string;
    private gold: number;

    constructor(gender: string, nickname: string, race: string, profession: string, gold: number)
    {
        this.gender = gender;
        this.nickname = nickname;
        this.race = race;
        this.profession = profession;
        this.gold = gold;
        this.gold = 0;
    }
    public deposito(valor: number) :void
    {
        if (valor >= 0) 
        {
            this.gold += valor;
        }
    }

    public entrada_ou_saida_de_gold(valor: number): void
    {   
        if (valor >= 0){
            this.gold += valor;
        }
        else 
        {   
            if (this.gold >= valor)
            {
                this.gold -= valor;
            }
            else
            {
                console.log("Impossível realizar operação. Não pode gastar mais do que tem");
            }

        }
    }

    public saldo_gold(): number 
    {
        return this.gold;
    }

}



